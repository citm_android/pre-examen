package ramos.julian.namelistapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class EditTextActivity extends AppCompatActivity {

    private EditText edit_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);

        edit_text = (EditText) findViewById(R.id.edit_title);

        Intent intent = getIntent();
        String text = intent.getStringExtra("text"); //agafa valor de text passat de l'altre intent
        if (text!=null){
            edit_text.setText(text);
        }


    }

    public void onSave(View view) {
        Intent data = new Intent();
        data.putExtra("text", edit_text.getText().toString());
        setResult(RESULT_OK, data);
        finish();
    }

}
