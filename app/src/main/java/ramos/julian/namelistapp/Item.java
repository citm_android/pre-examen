package ramos.julian.namelistapp;

import java.util.Date;

/**
 * Created by pablofd on 13/03/2018.
 */

public class Item {
    private String text;
    private boolean checked;
    private Date date = new Date();

    public Item(String text, boolean checked) { //quan crea va a aquí
        this.text = text;
        this.checked = checked;

    }

    public Item(String text) {
        this.text = text;
        this.checked = false;

        // Obtenir un instant de temps
         //date = new Date(); // agafa l'instant actual del dispositiu


    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void toggleChecked() {
        this.checked = !this.checked;
    }

    public Date getDate() {
        return date;
    }


}
