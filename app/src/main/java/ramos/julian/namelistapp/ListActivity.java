package ramos.julian.namelistapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class ListActivity extends AppCompatActivity {

    public static final String FILENAME = "items.txt";
    public static final int MAX_BYTES = 10000;
    private ListView list;
    private ArrayList<Item> items; // Model de dades
    private ListAdapter adapter;
    private EditText new_item;
    private TextView text;




    private void writeItemList() {
        try {
            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
            for (Item item : items) {
                String line = String.format("%s;%b\n", item.getText(), item.isChecked());
                fos.write(line.getBytes());
            }
            fos.close();
        }
        catch (FileNotFoundException e) {

        }
        catch (IOException e) {
            Toast.makeText(this, "No puc escriure el fitxer", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean readItemList() {
        items = new ArrayList<>();
        try {
            FileInputStream fis = openFileInput(FILENAME);
            byte[] buffer = new byte[MAX_BYTES];
            int nread = fis.read(buffer);
            if (nread > 0) {
                String content = new String(buffer, 0, nread);
                String[] lines = content.split("\n");
                for (String line : lines) {
                    if (!line.isEmpty()) {
                        String[] parts = line.split(";");
                        items.add(new Item(parts[0], parts[1].equals("true")));
                    }
                }
            }
            fis.close();
            return true;
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            Toast.makeText(this, "No puc llegir el fitxer", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        writeItemList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        if (!readItemList()) {
            Toast.makeText(this, "Benvingut al ShoppingList(TM)", Toast.LENGTH_SHORT).show();
        }

        list = (ListView) findViewById(R.id.list);
        new_item = (EditText) findViewById(R.id.new_item);

        adapter = new ListAdapter(this, R.layout.item, items);
        list.setAdapter(adapter);
//al clicar un cop va aquí
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                items.get(pos).toggleChecked();
                adapter.notifyDataSetChanged();
                String item_text = items.get(pos).getText().toString();
                Toast.makeText(ListActivity.this,item_text, Toast.LENGTH_SHORT).show();


               // edit(item_text,pos);//envia posicion y texto de item
            }
        });
//al mantenir el click va aqí
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                String item_text = items.get(pos).getText().toString();
                edit(item_text,pos);//envia posicion y texto de item
                return true;
            }
        });



    }

    private void onRemoveItem(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirm);
        builder.setMessage(
                String.format(Locale.getDefault(),
                        "Estàs segur que vols esborrar '%s'",
                        items.get(pos).getText())
        );
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                items.remove(pos);
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    public void onAddItem(View view) {
        String item_text = "Holi";
        if (!item_text.isEmpty()) {


            items.add(new Item(item_text));
            adapter.notifyDataSetChanged();
            new_item.setText("");

            list.smoothScrollToPosition(items.size() - 1);


        }
    }

    public void RemoveButton(View view) {
        onRemoveItem(0);
    }

    private void edit(String text, int requestCode){
        Intent intent = new Intent(this, EditTextActivity.class);//va a edit text
        intent.putExtra("text", text);//envia el text al altre activity en text
        startActivityForResult(intent, requestCode);
    }







    //rep les dades
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            String text = data.getStringExtra("text");
            items.get(requestCode).setText(text);
            adapter.notifyDataSetChanged();
            Log.i("entra", text);
        }
    }
}



