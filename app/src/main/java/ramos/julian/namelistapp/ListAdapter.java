package ramos.julian.namelistapp;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by pablofd on 06/03/2018.
 */

public class ListAdapter extends ArrayAdapter<Item> {
    //private Context c;




    public ListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Item> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // 1. Crear un nou View si és necessari (no cal si convertView no és null)
        View root = convertView; // arrel d'un item de la llista
        if (root == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            root = inflater.inflate(R.layout.item, parent, false);
        }
        final TextView edit_text =  root.findViewById(R.id.title_item);
        final Item item = getItem(position);
        edit_text.setText(item.getText());




        return root;
    }





}
